#!/usr/bin/env bash

export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://casper.a.openlmis.org:2376"
export DOCKER_CERT_PATH="$credentials"

../shared/pull_images.sh

../shared/restart.sh

sleep 300
docker pull openlmis/demo-data:casper
/usr/bin/docker run --rm --env-file settings.env openlmis/demo-data:casper
