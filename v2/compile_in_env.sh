#!/usr/bin/env bash
set -e

export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://casper-elmis.a.openlmis.org:2376"
export DOCKER_CERT_PATH=$credentials

cd ./build

docker-compose -f docker-compose.builder.yml build baseimage buildimage
