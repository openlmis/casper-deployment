#/bin/sh
set -e

eval `ssh-agent -s`
mkdir -p ~/.ssh
touch ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

git clone --single-branch --branch $TAG_BUILD https://gitlab.com/openlmis/casper-elmis.git build/

# Note: this file in the Gitlab UI must have a newline at the end!
chmod 0600 $ID_CASPER_RO
ssh-add $ID_CASPER_RO
ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
git clone --single-branch --branch $TAG_SRC git@bitbucket.org:elmis-tzm/open-lmis.git
cp -r open-lmis/. build/docker/builder/src/
rm -rf open-lmis
cd build/docker/builder/src/
git checkout $TAG_V2_COMMIT
