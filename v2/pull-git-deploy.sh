#/bin/sh
set -e

eval `ssh-agent -s`
mkdir -p ~/.ssh
touch ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

git clone --single-branch --branch $TAG_BUILD https://gitlab.com/openlmis/casper-elmis.git build/
