#!/usr/bin/env bash
set -e

export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://casper-elmis.a.openlmis.org:2376"
export DOCKER_CERT_PATH=$credentials

cd ./build

SAVED_DB_DUMP=${PWD}/docker/main/saved_db.dump
touch $SAVED_DB_DUMP

# If we are doing an in-place upgrade, get a database dump from the running
# container before destroying it and making the new one
if [ $KEEP_OR_WIPE = "keep" ]; then
    NUM_CONTAINERS=`docker ps | grep "casper/elmis" | cut -d " " -f 1 | wc -l`
    if (( $NUM_CONTAINERS < 1 )); then
        echo "No running casper/elmis container to restore state from."
        exit 1
    fi
    if (( $NUM_CONTAINERS > 1 )); then
        echo "There are $NUM_CONTAINERS running casper/elmis containers. Using \
              the first one."
    fi

    CONTAINER_ID=`docker ps | grep "casper/elmis" | cut -d " " -f 1 | head -1`
    docker exec -u postgres $CONTAINER_ID /usr/bin/pg_dump open_lmis  > $SAVED_DB_DUMP
fi

docker-compose down -v
docker-compose -f docker-compose.builder.yml build \
    --build-arg KEEP_OR_WIPE=$KEEP_OR_WIPE \
    --build-arg SAVED_DB_DUMP=saved_db.dump \
    image
docker network inspect casper_elmis_pipeline_bridge &>/dev/null || docker network create --driver bridge casper_elmis_pipeline_bridge
docker-compose up -d
