# How to confirm the NiFi registry is working
The NiFi registry has a web GUI that should be accessible at `http://hostname:18080/nifi-registry/explorer/grid-list`, which in this case is [http://casper-nifi-registry.a.openlmis.org].

On start up, the web GUI should show half a dozen or more flows, e.g. "Requisitions Connector - Reporting".
