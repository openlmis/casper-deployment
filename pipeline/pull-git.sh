#/bin/sh
set -e

eval `ssh-agent -s`
mkdir -p ~/.ssh
touch ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

git clone --single-branch https://gitlab.com/openlmis/casper-pipeline.git

chmod 0600 $VILLAGEREACH_SSH_KEY
ssh-add $VILLAGEREACH_SSH_KEY
ssh-keyscan github.com >> ~/.ssh/known_hosts
git clone --single-branch git@github.com:villagereach/openlmis-config.git .deployment-config
