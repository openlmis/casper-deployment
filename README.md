Casper deployment
=================

This repository defines build & deployment jobs for casper, using Gitlab's
CI/CD tools.

`.gitlab-ci.yml` is the main file in this repository and is where Gitlab looks
for job definitions. All of the jobs here are marked with `when: manual` to
disable the default behavior that runs all jobs on every commit.

Run jobs by going to CI / CD -> Pipelines. There, you can run jobs at a specific
commit in this repo by selecting the job you want from the dropdown to the right
of that commit.

Configuring Gitlab
------------------

The jobs defined here expect some environment variables, which are defined under
Settings -> CI / CD -> Variables. All of these should be set to "File" in the
Gitlab UI. **All of these should end in a newline**
 - `ID_CASPER_RO` - SSH private key for bitbucket.org/elmis-tzm/open-lmis
 - `VILLAGEREACH_SSH_KEY` - SSH private key for github.com/villagereach/openlmis-config
 - `docker_elmis_ca_pem` - Certificate (ca.pem) for docker TLS connection to v2
 - `docker_elmis_cert_pem` - Certificate (cert.pem) for docker TLS connection to v2
 - `docker_elmis_key_pem` - Certificate (key.pem) for docker TLS connection to v2

