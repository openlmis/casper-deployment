#!/bin/bash
set -e

# The file ca.pem is stored in a Gitlab environment variable with a name like
# docker_elmis_ca_pem
# We're using a bash trick called indirect expansion here. sh doesn't have it:
# https://stackoverflow.com/questions/47592506/what-does-exclamatory-mark-inside-curly-braces-when-using-variable-in-u
cd $credentials
tmp=$1_ca_pem; cp ${!tmp} ca.pem
tmp=$1_cert_pem; cp ${!tmp} cert.pem
tmp=$1_key_pem; cp ${!tmp} key.pem
